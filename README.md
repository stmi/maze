# Maze

Maze Generator written in Java

## Compile & Run

In root directory execute

```shell
mvn verify
```

Example generation request
```shell
java -jar command-line-tool/target/maze.jar -w 10 -h 10 -o maze.svg
```

This will output 10 by 10 nodes maze to `maze.svg` file

![Example 10 by 10 Maze](docs/maze.svg)*Example 10 by 10 Maze*
