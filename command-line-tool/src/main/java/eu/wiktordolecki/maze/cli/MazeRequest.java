package eu.wiktordolecki.maze.cli;

import eu.wiktordolecki.maze.LocationPreference;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;
import java.util.Optional;

@Getter
@Setter
@ToString
@Command(name = "maze",
        sortOptions = false,
        footer = "Copyright(c) 2022",
        description = "Maze Generator CLI Tool: Generate maze on a square grid.")
public class MazeRequest {

    @Positive(message = "Width should be positive number between 1 and 200")
    @Min(value = 1, message = "Width should not be less than 1")
    @Max(value = 200, message = "Width should not be greater than 200")
    @Option(names = {"-wd", "--width"}, required = true, description = "Width of requested maze")
    private int width;

    @Positive(message = "Height should be positive number between 1 and 200")
    @Min(value = 1, message = "Height should not be less than 1")
    @Max(value = 200, message = "Height should not be greater than 200")
    @Option(names = {"-ht", "--height"}, required = true, description = "Height of requested maze")
    private int height;

    @Option(names = {"-o", "--output"}, required = true, description = "Output file name")
    private String outputFileName;

    @Positive(message = "Tile Size should be positive number between 10 and 100")
    @Min(value = 10, message = "Tile Size should not be less than 10")
    @Max(value = 100, message = "Tile Size should not be greater than 100")
    @Option(names = {"-ts", "--tile-size"}, description = "Tile Size in pixels for requested maze")
    private Optional<Integer> tileSize;

    @Option(names = {"-s", "--start-position"}, description = "Location of start tile")
    private Optional<LocationPreference> startPos;

    @Option(names = {"-t", "--target-position"}, description = "Location of target tile")
    private Optional<LocationPreference> targetPos;

    @Option(names = {"-rs", "--random-seed"}, description = "Value of the random seed for generation")
    private Optional<Long> seed;

    @Option(names = {"-sol", "--solution"}, description = "Draw maze solution")
    private boolean showSolution = false;

    @Option(names = {"-h", "--help"}, usageHelp = true, description = "display a help message")
    private boolean helpRequested = false;
}
