package eu.wiktordolecki.maze;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Random;

public class RandomizedQueue<Item> implements Iterable<Item> {

    private static final int MIN_CAPACITY = 16;

    private Item[] items;
    private int counter;
    private int capacity;

    private final Random random;

    /**
     * Construct an empty randomized queue
     */
    public RandomizedQueue(Random random) {
        this.items = (Item[]) new Object[MIN_CAPACITY];
        this.counter = 0;
        this.capacity = MIN_CAPACITY;
        this.random = random;
    }

    /**
     * Is the queue empty?
     */
    public boolean isEmpty() {
        return counter == 0;
    }

    /**
     * Return the number of items on the queue.
     */
    public int size() {
        return counter;
    }

    /**
     * Add the item.
     */
    public void enqueue(Item item) {
        if (item == null) throw new NullPointerException();
        if (isFull()) increaseSize();

        items[counter++] = item;
    }

    /**
     * Delete and return a random item.
     */
    public Item dequeue() {
        if (counter <= 0) throw new NoSuchElementException();

        int idx = random.nextInt(counter);

        Item item = items[idx];
        items[idx] = items[--counter];
        items[counter] = null;

        if (lessThanQuarter()) decreaseSize();

        return item;
    }

    /**
     * Return (but do not delete) a random item.
     */
    public Item sample() {
        if (counter <= 0) throw new NoSuchElementException();

        int idx = random.nextInt(counter);

        return items[idx];
    }

    /**
     * Return an independent iterator over items in random order.
     */
    public Iterator<Item> iterator() {
        Item[] copy = (Item[]) new Object[capacity];

        for (int i = 0; i < items.length; i++) {
            copy[i] = items[i];
        }

        return new RandomizedQueueIterator(copy, counter);
    }

    private boolean isFull() {
        return counter == capacity;
    }

    private boolean lessThanQuarter() {
        return capacity > MIN_CAPACITY && counter < capacity / 4;
    }

    private void increaseSize() {
        capacity *= 2;
        rewriteArray();
    }

    private void decreaseSize() {
        capacity /= 2;
        rewriteArray();
    }

    private void rewriteArray() {
        Item[] nextArray = (Item[]) new Object[capacity];

        for (int i = 0; i < counter; i++) {
            nextArray[i] = items[i];
        }

        items = nextArray;
    }

    private class RandomizedQueueIterator implements Iterator<Item> {

        private Item[] items;
        private int counter;

        RandomizedQueueIterator(Item[] items, int counter) {
            this.items = items;
            this.counter = counter;
        }

        @Override
        public boolean hasNext() {
            return counter > 0;
        }

        @Override
        public Item next() {
            if (counter <= 0) throw new NoSuchElementException();

            int idx = random.nextInt(counter);
            Item item = items[idx];
            counter--;
            items[idx] = items[counter];
            items[counter] = null;

            return item;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}


