package eu.wiktordolecki.maze;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.xmlunit.builder.Input;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;

import static org.xmlunit.assertj3.XmlAssert.assertThat;

class MazeRendererTest {

    public static final int TILE_SIZE = 25;
    public static final String REFERENCE_MAZE_SVG_FILE_NAME = "reference_maze.svg";
    private MazeRenderer renderer;

    @BeforeEach
    void setUp() {
        renderer = new MazeRenderer().tileSize(TILE_SIZE);
    }

//    @Test
    public void testRendererOutput() throws IOException {
        Random random = new Random(12345);
        MazeBuilder builder = new MazeBuilder(random);
        SquareMaze maze = builder.buildMaze(10, 10);

        String actual = renderer.renderToSvg(maze);

        Path input_path = Paths.get("src", "test", "resources", REFERENCE_MAZE_SVG_FILE_NAME);

        assertThat(actual)
                .and(Input.fromPath(input_path))
                .ignoreWhitespace()
                .areSimilar();
    }
}