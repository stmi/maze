package eu.wiktordolecki.maze;

import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.constraints.IntRange;

import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

class MazeBuilderTest {

    @Property
    void negativeHeightThrowsException(
            @ForAll @IntRange(min = 1, max = 200) int width,
            @ForAll @IntRange(min = 1, max = 200) int height,
            @ForAll Random random) {
        MazeBuilder builder = new MazeBuilder(random);
        builder.setSeed(random.nextInt());

        SquareMaze maze = builder.buildMaze(width, height);

        assertThat(maze).isNotNull();
    }
}