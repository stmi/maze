package eu.wiktordolecki.maze;

import eu.wiktordolecki.maze.SquareMaze.SquareTile;
import net.jqwik.api.*;
import net.jqwik.api.Tuple.Tuple2;
import net.jqwik.api.arbitraries.IntegerArbitrary;
import net.jqwik.api.constraints.Negative;
import net.jqwik.api.constraints.Positive;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class SquareMazeTest {

    @Provide
    Arbitrary<SquareMaze> arbitrarySquareGrid() {
        IntegerArbitrary width = Arbitraries.integers().between(2, 256);
        IntegerArbitrary height = Arbitraries.integers().between(2, 256);

        return Combinators.combine(width, height).as(SquareMaze::new);
    }

    @Provide
    Arbitrary<Tuple2<SquareMaze, SquareTile>> TilePair() {
        return arbitrarySquareGrid().flatMap(grid -> {
            IntegerArbitrary x = Arbitraries.integers().between(0, grid.width() - 1);
            IntegerArbitrary y = Arbitraries.integers().between(0, grid.height() - 1);

            return Combinators.combine(x, y).as(grid::at).map(tile -> Tuple.of(grid, tile));
        });
    }

    @Provide
    Arbitrary<Tuple2<SquareMaze, SquareTile>> gridTopTilePair() {
        return arbitrarySquareGrid().flatMap(grid -> {
            IntegerArbitrary x = Arbitraries.integers().between(0, grid.width() - 1);
            IntegerArbitrary y = Arbitraries.integers().between(0, 0);

            return Combinators.combine(x, y).as(grid::at).map(tile -> Tuple.of(grid, tile));
        });
    }

    @Provide
    Arbitrary<Tuple2<SquareMaze, SquareTile>> gridBottomTilePair() {
        return arbitrarySquareGrid().flatMap(grid -> {
            IntegerArbitrary x = Arbitraries.integers().between(0, grid.width() - 1);
            IntegerArbitrary y = Arbitraries.integers().between(grid.height() - 1, grid.height() - 1);

            return Combinators.combine(x, y).as(grid::at).map(tile -> Tuple.of(grid, tile));
        });
    }

    @Provide
    Arbitrary<Tuple2<SquareMaze, SquareTile>> gridLeftTilePair() {
        return arbitrarySquareGrid().flatMap(grid -> {
            IntegerArbitrary x = Arbitraries.integers().between(0, 0);
            IntegerArbitrary y = Arbitraries.integers().between(0, grid.height() - 1);

            return Combinators.combine(x, y).as(grid::at).map(tile -> Tuple.of(grid, tile));
        });
    }

    @Provide
    Arbitrary<Tuple2<SquareMaze, SquareTile>> gridRightTilePair() {
        return arbitrarySquareGrid().flatMap(grid -> {
            IntegerArbitrary x = Arbitraries.integers().between(grid.width() - 1, grid.width() - 1);
            IntegerArbitrary y = Arbitraries.integers().between(0, grid.height() - 1);

            return Combinators.combine(x, y).as(grid::at).map(tile -> Tuple.of(grid, tile));
        });
    }

    @Property
    void negativeWidthThrowsException(
            @ForAll @Negative int width,
            @ForAll @Positive int height) {
        assertThatThrownBy(() -> {
            new SquareMaze(width, height);
        })
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("SquareGrid width has to be positive integer");
    }

    @Example
    void zeroWidthThrowsException(@ForAll @Positive int height) {
        assertThatThrownBy(() -> {
            new SquareMaze(0, height);
        })
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("SquareGrid width has to be positive integer");
    }

    @Property
    void negativeHeightThrowsException(
            @ForAll @Positive int width,
            @ForAll @Negative int height) {
        assertThatThrownBy(() -> {
            new SquareMaze(width, height);
        })
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("SquareGrid height has to be positive integer");
    }

    @Example
    void zeroHeightThrowsException(@ForAll @Positive int width) {
        assertThatThrownBy(() -> {
            new SquareMaze(width, 0);
        })
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("SquareGrid height has to be positive integer");
    }

    @Property
    void borderCheckFalseInsideGrid(@ForAll("TilePair") Tuple2<SquareMaze, SquareTile> tuple) {
        SquareMaze maze = tuple.get1();
        SquareTile tile = tuple.get2();

        Assume.that(maze.width() >= 3);
        Assume.that(maze.height() >= 3);

        Assume.that(tile.x() > 0 && tile.x() < maze.width() - 1);
        Assume.that(tile.y() > 0 && tile.y() < maze.height() - 1);

        assertThat(tile.isBorder()).isFalse();
    }

    @Property
    void upAndDownAreReversibleBellowTop(@ForAll("TilePair") Tuple2<SquareMaze, SquareTile> tuple) {
        SquareTile tile = tuple.get2();
        Assume.that(tile.y() > 0);

        SquareTile upTile = tile.up();
        assertThat(upTile.down()).isSameAs(tile);
    }

    @Property
    void upOnTopRowMovesOffGrid(@ForAll("gridTopTilePair") Tuple2<SquareMaze, SquareTile> tuple) {
        SquareTile tile = tuple.get2();
        Assume.that(tile.y() == 0);

        assertThat(tile.up()).isNull();
        assertThat(tile.isBorder()).isTrue();
    }

    @Property
    void downAndUpAreReversibleAboveBottom(@ForAll("TilePair") Tuple2<SquareMaze, SquareTile> tuple) {
        SquareMaze grid = tuple.get1();
        SquareTile tile = tuple.get2();
        Assume.that(tile.y() < grid.height() - 1);

        SquareTile downTile = tile.down();
        assertThat(downTile.up()).isSameAs(tile);
    }

    @Property
    void downOnBottomRowMovesOffGrid(@ForAll("gridBottomTilePair") Tuple2<SquareMaze, SquareTile> tuple) {
        SquareTile tile = tuple.get2();

        assertThat(tile.down()).isNull();
        assertThat(tile.isBorder()).isTrue();
    }

    @Property
    void leftAndRightAreReversiblePastLeftMargin(@ForAll("TilePair") Tuple2<SquareMaze, SquareTile> tuple) {
        SquareTile tile = tuple.get2();
        Assume.that(tile.x() > 0);

        SquareTile leftTile = tile.left();
        assertThat(leftTile.right()).isSameAs(tile);
    }

    @Property
    void leftOnLeftMarginMovesOffGrid(@ForAll("gridLeftTilePair") Tuple2<SquareMaze, SquareTile> tuple) {
        SquareTile tile = tuple.get2();

        assertThat(tile.left()).isNull();
        assertThat(tile.isBorder()).isTrue();
    }

    @Property
    void rightAndLeftAreReversibleBeforeRightMargin(@ForAll("TilePair") Tuple2<SquareMaze, SquareTile> tuple) {
        SquareMaze grid = tuple.get1();
        SquareTile tile = tuple.get2();
        Assume.that(tile.x() < grid.width() - 1);

        SquareTile rightTile = tile.right();
        assertThat(rightTile.left()).isSameAs(tile);
    }

    @Property
    void rightOnRightMarginMovesOffGrid(@ForAll("gridRightTilePair") Tuple2<SquareMaze, SquareTile> tuple) {
        SquareTile tile = tuple.get2();

        assertThat(tile.right()).isNull();
        assertThat(tile.isBorder()).isTrue();
    }

    @Example
    void movingInAnyDirectionOnGridSizeOneMovesOffGrid() {
        SquareMaze grid = new SquareMaze(1, 1);
        SquareTile tile = grid.at(0, 0);

        assertThat(tile.up()).isNull();
        assertThat(tile.down()).isNull();
        assertThat(tile.left()).isNull();
        assertThat(tile.right()).isNull();

        assertThat(tile.isBorder()).isTrue();
    }

    @Example
    void cannotConnectTilesFromDistinctMazes() {
        SquareMaze maze1 = new SquareMaze(10, 10);
        SquareMaze maze2 = new SquareMaze(10, 10);

        SquareTile tile1 = maze1.at(1, 2);
        SquareTile tile2 = maze2.at(3, 4);
        tile1.connect(tile2);

        assertThat(tile1.isConnected(tile2)).isFalse();
        assertThat(tile1.neighbours()).isEmpty();
        assertThat(tile2.neighbours()).isEmpty();
    }

    @Example
    void tileCannotConnectToSelf() {
        SquareMaze maze = new SquareMaze(10, 10);
        SquareTile tile = maze.at(5, 5);

        tile.connect(tile);

        assertThat(tile.isConnected(tile)).isFalse();
        assertThat(tile.neighbours()).isEmpty();
    }

    @Example
    void tileConnectingToNullDoesntThrow() {
        SquareMaze maze = new SquareMaze(10, 10);

        SquareTile tile = maze.at(5, 5);
        tile.connect(null);

        assertThat(tile.neighbours()).isEmpty();
    }

    @Example
    void aroundOnGridSizeOneGivesNoElements() {
        SquareMaze grid = new SquareMaze(1, 1);
        SquareTile tile = grid.at(0, 0);

        assertThat(tile.aroundStream().count()).isZero();
    }
    
    @Example
    void toStringIsProvidingExpectedOutput() {
        SquareMaze grid = new SquareMaze(1, 1);

        String expected = "SquareGrid{width=1, height=1}";

        assertThat(grid.toString()).isEqualTo(expected);
    }
}