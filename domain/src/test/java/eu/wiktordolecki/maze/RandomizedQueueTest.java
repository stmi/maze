package eu.wiktordolecki.maze;

import net.jqwik.api.Example;
import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.constraints.IntRange;
import net.jqwik.api.constraints.Size;
import net.jqwik.api.constraints.UniqueElements;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class RandomizedQueueTest {

    private RandomizedQueue<Integer> queue;

    @Property
    void randomizedQueueIteratorAlwaysReturnsAllElementsEnqueued(
            @ForAll @Size(min = 1, max = 50) @UniqueElements
            List<@IntRange(min = 0, max = 200) Integer> integers,
            @ForAll Random random
    ) {
        queue = new RandomizedQueue<>(random);

        integers.forEach(queue::enqueue);
        assertThat(queue.size()).isEqualTo(integers.size());

        List<Integer> actual = new ArrayList<>();
        Iterator<Integer> iterator = queue.iterator();
        iterator.forEachRemaining(actual::add);

        assertThat(actual).containsExactlyInAnyOrderElementsOf(integers);

        assertThatThrownBy(
                () -> iterator.next()
        ).isInstanceOf(NoSuchElementException.class);
    }

    @Property
    void randomizedQueueSamplesOnlyElementsEnqueued(
            @ForAll @Size(min = 1, max = 50) @UniqueElements
            List<@IntRange(min = 0, max = 200) Integer> integers,
            @ForAll Random random
    ) {
        queue = new RandomizedQueue<>(random);

        integers.forEach(queue::enqueue);
        assertThat(queue.size()).isEqualTo(integers.size());

        assertThat(integers).contains(queue.sample());
    }

    @Example
    void enqueueNullThrowsException(@ForAll Random random) {
        queue = new RandomizedQueue<>(random);

        assertThatThrownBy(() ->
                queue.enqueue(null)
        ).isInstanceOf(NullPointerException.class);
    }

    @Example
    void emptyQueueNotSupportingSampleAndDeque(@ForAll Random random) {
        queue = new RandomizedQueue<>(random);

        assertThatThrownBy(
                () -> queue.sample()
        ).isInstanceOf(NoSuchElementException.class);

        assertThatThrownBy(
                () -> queue.dequeue()
        ).isInstanceOf(NoSuchElementException.class);
    }

    @Example
    void iteratorNotSupportingRemove(
            @ForAll @Size(min = 1, max = 50) @UniqueElements
            List<@IntRange(min = 0, max = 200) Integer> integers,
            @ForAll Random random) {
        queue = new RandomizedQueue<>(random);

        integers.forEach(queue::enqueue);

        assertThatThrownBy(() ->
                queue.iterator().remove()
        ).isInstanceOf(UnsupportedOperationException.class);
    }
}